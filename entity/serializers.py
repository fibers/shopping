from django.core.serializers import serialize
from entity.models import User, Merchant, Clip
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'gender')

class MerchantSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Merchant
        fields = ('url', 'name')

class ClipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Clip
        fields = ('url', 'title', 'merchant')