from django.contrib import admin
from entity.models import User, Merchant, Clip

# Register your models here.

admin.site.register(User)
admin.site.register(Merchant)
admin.site.register(Clip)