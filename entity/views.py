from django.shortcuts import render
from entity.models import User, Merchant, Clip
from entity.serializers import UserSerializer, MerchantSerializer, ClipSerializer
from rest_framework import viewsets

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed and edited
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class MerchantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows merchants to be viewed and edited
    """
    queryset = Merchant.objects.all()
    serializer_class = MerchantSerializer

class ClipViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows clips to be viewed and edited
    """
    queryset = Clip.objects.all()
    serializer_class = ClipSerializer