from django.db import models

# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=30)
    gender = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Merchant(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Clip(models.Model):
    title = models.CharField(max_length=30)
    merchant = models.ForeignKey(Merchant)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)