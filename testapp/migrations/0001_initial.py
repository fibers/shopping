# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Test'
        db.create_table(u'testapp_test', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('test_name', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('test_name2', self.gf('django.db.models.fields.CharField')(default=None, max_length=30)),
            ('test_int', self.gf('django.db.models.fields.IntegerField')()),
            ('test_int2', self.gf('django.db.models.fields.IntegerField')(default=None)),
        ))
        db.send_create_signal(u'testapp', ['Test'])


    def backwards(self, orm):
        # Deleting model 'Test'
        db.delete_table(u'testapp_test')


    models = {
        u'testapp.test': {
            'Meta': {'object_name': 'Test'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'test_int': ('django.db.models.fields.IntegerField', [], {}),
            'test_int2': ('django.db.models.fields.IntegerField', [], {'default': 'None'}),
            'test_name': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'test_name2': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30'})
        }
    }

    complete_apps = ['testapp']