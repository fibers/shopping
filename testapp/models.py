from django.db import models

# Create your models here.


class Test(models.Model):
    test_name = models.CharField(max_length=20, null=True)
    test_name2 = models.CharField(max_length=30, default=None)
    test_int = models.IntegerField(null=True)
    test_int2 = models.IntegerField(default=None)