from django.conf.urls import patterns, include, url
from rest_framework import routers
from django.contrib import admin
from entity import views

admin.autodiscover()

# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'merchants', views.MerchantViewSet)
router.register(r'clips', views.ClipViewSet)

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'shopping.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^', include(router.urls)),
                       url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                       url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
                       url(r'^api-token-refresh/', 'rest_framework_jwt.views.refresh_jwt_token'),
)
